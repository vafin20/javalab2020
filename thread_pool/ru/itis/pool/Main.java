package ru.itis.pool;

public class Main {
    public static void main(String[] args) {
        ThreadPool threadPool = new ThreadPool(3);

        threadPool.submit(() -> {
            for (int i = 0; i < 2; i++) {
                System.out.println(Thread.currentThread().getName() + " " + "A");
            }
        });

//        threadPool.submit(() -> {
//            for (int i = 0; i < 2; i++) {
//                System.out.println(Thread.currentThread().getName() + " " + "B");
//            }
//        });
//
//        threadPool.submit(() -> {
//            for (int i = 0; i < 2; i++) {
//                System.out.println(Thread.currentThread().getName() + " " + "C");
//            }
//        });
//
//        threadPool.submit(() -> {
//            for (int i = 0; i < 2; i++) {
//                System.out.println(Thread.currentThread().getName() + " " + "D");
//            }
//        });
    }
}

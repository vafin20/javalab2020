bodyParser = require('body-parser').json();
fs = require('fs');

module.exports = function (app) {
    app.get('/profile', (request, response) => {
        var result = [{
            "name": "info",
            "title": "Откуда ты вообще взялся?",
            "data": "С самых малых лет я не слезал с компа брата," +
                " пока меня не прогоняли с него, рубился в GTA San Andreas еще в далеком 2006––2007 году и " +
                "не мог пройти миссию с велосипедами на районе Сиджея." +
                " С 1 по 11 класс я учился в Казанской Гимназии №19, поначалу хотел стать дипломатом," +
                " потом врачом, но страсть к компьютерам одолела меня в 9 классе. " +
                "С тех пор я начал заниматься разработкой " +
                "и даже успел поработать программистом, но это совсем другая история."
        },
            {
                "name": "skills",
                "title": "Что я умею?",
                "data": "Я \"знаю\" такие языки программирования как Java, TypeScript, ну и остальные(немношк).\n" +
                    "С TypeScript я работал очень тесно, разрабатывая middle-end приложение на фреймворке NestJS."
            }];
        response.send(JSON.stringify(result));
    });

     app.post('/register', bodyParser, (request, response) => {
       let body = request.body;
       console.log(body);
       fs.readFile('forms.json', 'utf-8', function(err, data) {
           if (err) throw err;

           var arrayOfObjects = JSON.parse(data);
           arrayOfObjects.forms.push(body);


           fs.writeFile('forms.json', JSON.stringify(arrayOfObjects), 'utf-8', function(err) {
               if (err) throw err;
               console.log('Done!')
           })
       });
        response.redirect(200, '/');
    });

    app.get('/forms', (request, response) => {
        fs.readFile('forms.json', 'utf8', function(err, contents) {
            response.setHeader('Content-Type', 'application/json');
            response.send(contents);
        });
    });
};

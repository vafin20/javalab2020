package ru.itis.repo;

import ru.itis.models.Mentor;
import ru.itis.models.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //Language=SQL
    private static final String SQL_SELECT_BY_ID = "SELECT * FROM student WHERE id =";
    private static final String SQL_SELECT_BY_AGE = "SELECT * FROM student WHERE age =";
    private static final String SQL_INSERT_STUDENT = "INSERT INTO student " +
            "(first_name, last_name, age, group_number) VALUES ";
    private static final String UPDATE_F = "UPDATE student SET first_name = ";
    private static final String UPDATE_L = ", last_name = ";
    private static final String UPDATE_AGE = ", age = ";
    private static final String UPDATE_GR = ", group_number = ";
    private static final String ID = " WHERE id = ";

    private Connection connection;

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {
        Statement statement = null;
        ResultSet result = null;
        List<Student> studentList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_AGE + age);
            while (result.next()) {
                studentList.add(new Student(
                        result.getLong("id"),
                        result.getString("first_name").trim(),
                        result.getString("last_name").trim(),
                        result.getInt("age"),
                        result.getInt("group_number")
                ));
            }
            return studentList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    @Override
    public List<Student> findall() {
        Statement statement = null;
        ResultSet result = null;
        List<Student> slist = new LinkedList<>();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery("SELECT * FROM student left join mentor m on student.id = m.student_id");
            while (result.next()) {
                Student student = new Student (
                        result.getLong("id"),
                        result.getString("first_name").trim(),
                        result.getString("last_name").trim(),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
                if (student.getMentors() == null) {
                    student.setMentors(new LinkedList<>());
                }
                if (result.getString("m_first_name") != null) {
                    Mentor mentor = new Mentor(
                            result.getLong("m_id"),
                            result.getString("m_first_name").trim(),
                            result.getString("m_last_name").trim()
                    );
                    if (!slist.contains(student)) {
                        List<Mentor> mlist = student.getMentors();
                        mlist.add(mentor);
                        student.setMentors(mlist);
                        slist.add(student);
                    } else for (Student s : slist) {
                        if (s.getId().equals(student.getId())) {
                            student.setMentors(s.getMentors());
                            slist.remove(s);
                            List<Mentor> mlist = student.getMentors();
                            mlist.add(mentor);
                            student.setMentors(mlist);
                            slist.add(student);
                        }
                    }
                }
                else slist.add(student);
            }
            return slist;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet resultSet =  null;

        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            if (resultSet.next()) {
                return new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("first_name").trim(),
                        resultSet.getString("last_name").trim(),
                        resultSet.getInt("age"),
                        resultSet.getInt("group_number")
                );
            } else return null;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public void save(Student entity) {
        Statement statement = null;
        Statement statement1 = null;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_INSERT_STUDENT + "(\'" + entity.getFirstName() + "\'," +
                    "\'" + entity.getLastName() + "\'," + entity.getAge() + "," + entity.getGroupNumber() +
                    ")"
            );
            statement1 = connection.createStatement();
            result = statement1.executeQuery("SELECT max(id) as i FROM student");
            if (result.next()) {
                long l = result.getLong("i");
                entity.setId(l);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    //ignore
                }
            }
        }
    }

    @Override
    public void update(Student entity) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(UPDATE_F + "'" +entity.getFirstName() + "'" + UPDATE_L + "'" + entity.getLastName() + "'" + UPDATE_AGE +
                    entity.getAge() + UPDATE_GR + entity.getGroupNumber() + ID + entity.getId());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    //ignore
                }
            }
        }

    }
}

package ru.itis.repo;

import java.util.List;

public interface CrudRepository<T> {
    List<T> findall();
    T findById(Long id);
    void save(T entity);
    void update(T entity);
}
